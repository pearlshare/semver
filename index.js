
function parse (version) {
  if (typeof version === 'object' && version.hasOwnProperty("major") && version.hasOwnProperty("minor") && version.hasOwnProperty("patch")) {
    return {
      major: parseInt(version.major),
      minor: parseInt(version.minor),
      patch: parseInt(version.patch)
    };
  } else if (typeof version == 'string') {
    var results = version.match(/^(\d*)\.(\d*)\.(\d*)$/);

    if (results) {
      return {
        major: +results[1],
        minor: +results[2],
        patch: +results[3]
      }
    } else {
      return {};
    }
  }
}

function compare (one, two) {

}

module.exports = (function () {

  function Semver (version) {
    var semver = parse(version);
    this.major = semver.major;
    this.minor = semver.minor;
    this.patch = semver.patch;
  }

  Semver.prototype.gt = function (test) {
    var test = parse(test)
    return (this.major > test.major) ||
           (this.major === test.major && this.minor > test.minor) ||
           (this.major === test.major && this.minor === test.minor && this.patch > test.patch)
  }

  Semver.prototype.gte = function (test) {
    return this.gt(test) || this.eql(test);
  }

  Semver.prototype.lt = function (test) {
    var test = parse(test)
    return (this.major < test.major) ||
           (this.major === test.major && this.minor < test.minor) ||
           (this.major === test.major && this.minor === test.minor && this.patch < test.patch)
  }

  Semver.prototype.lte = function (test) {
    return this.lt(test) || this.eql(test);
  }

  Semver.prototype.eql = function (test) {
    var test = parse(test)
    return this.major == test.major && this.minor === test.minor && this.patch === test.patch;
  }

  return Semver;
})()
