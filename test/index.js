var Semver = require("../index");
var version = '1.22.288';
var expect = require("expect.js");

describe('Semver', function() {
  var semver = new Semver(version);

  it('should have major, minor and patch', function() {
    expect(semver.major).to.equal(1);
    expect(semver.minor).to.equal(22);
    expect(semver.patch).to.equal(288);
  });

  describe('some test', function () {
    var one = new Semver('1.0.24')
    var two = new Semver('9999.0.0')

    it('one should not be eql two', function () {
      expect(one.eql(two)).to.be(false)
    });
    it('one should not be gt two', function () {
      expect(one.gt(two)).to.be(false)
    });
    it('one should not be gte two', function () {
      expect(one.gte(two)).to.be(false)
    });
    it('one should be lt two', function () {
      expect(one.lt(two)).to.be(true)
    });
    it('one should be lte two', function () {
      expect(one.lte(two)).to.be(true)
    });
  });

  describe('gt', function() {
    it('should return true if given a 0 values', function() {
      expect(semver.gte('0.0.0')).to.be(true);
    });
    it('should return true if given a smaller semver', function() {
      expect(semver.gt('0.22.288')).to.be(true);
      expect(semver.gt('1.21.288')).to.be(true);
      expect(semver.gt('1.22.287')).to.be(true);
    });
    it('should return false if given the same semver', function() {
      expect(semver.gt(version)).to.be(false);
    });
    it('should return false if given a bigger semver', function() {
      expect(semver.gt('2.22.288')).to.be(false);
      expect(semver.gt('1.23.288')).to.be(false);
      expect(semver.gt('1.22.289')).to.be(false);
    });
  });

  describe('gte', function() {
    it('should return true if given a 0 values', function() {
      expect(semver.gte('0.0.0')).to.be(true);
    });
    it('should return true if given a smaller semver', function() {
      expect(semver.gte('0.22.288')).to.be(true);
      expect(semver.gte('1.21.288')).to.be(true);
      expect(semver.gte('1.22.287')).to.be(true);
    });
    it('should return true if given the same semver', function() {
      expect(semver.gte(version)).to.be(true);
    });
    it('should return false if given a bigger semver', function() {
      expect(semver.gte('2.22.288')).to.be(false);
      expect(semver.gte('1.23.288')).to.be(false);
      expect(semver.gte('1.22.289')).to.be(false);
    });
  });

  describe('lt', function() {
    it('should return false if given a 0 values', function() {
      expect(semver.lt('0.0.0')).to.be(false);
    });
    it('should return true if given a bigger semver', function() {
      expect(semver.lt('1.22.289')).to.be(true);
      expect(semver.lt('1.23.289')).to.be(true);
      expect(semver.lt('2.22.289')).to.be(true);
    });
    it('should return false if given the same semver', function() {
      expect(semver.lt(version)).to.be(false);
    });
    it('should return false if given a smaller semver', function() {
      expect(semver.lt('1.22.287')).to.be(false);
      expect(semver.lt('1.21.288')).to.be(false);
      expect(semver.lt('0.22.288')).to.be(false);
    });
  });

  describe('lte', function() {
    it('should return false if given a 0 values', function() {
      expect(semver.lt('0.0.0')).to.be(false);
    });
    it('should return true if given a bigger semver', function() {
      expect(semver.lte('1.22.289')).to.be(true);
      expect(semver.lte('1.23.289')).to.be(true);
      expect(semver.lte('2.22.289')).to.be(true);
    });
    it('should return true if given the same semver', function() {
      expect(semver.lte(version)).to.be(true);
    });
    it('should return false if given a smaller semver', function() {
      expect(semver.lte('1.22.287')).to.be(false);
      expect(semver.lte('1.21.288')).to.be(false);
      expect(semver.lte('0.22.288')).to.be(false);
    });
  });

  describe('eql', function() {
    it('should return false if given a 0 values', function() {
      expect(semver.lt('0.0.0')).to.be(false);
    });
    it('should return false if given a bigger semver', function() {
      expect(semver.eql('1.22.289')).to.be(false);
      expect(semver.eql('1.23.289')).to.be(false);
      expect(semver.eql('2.22.289')).to.be(false);
    });
    it('should return true if given the same semver', function() {
      expect(semver.eql(version)).to.be(true);
    });
    it('should return false if given a smaller semver', function() {
      expect(semver.eql('1.22.287')).to.be(false);
      expect(semver.eql('1.21.288')).to.be(false);
      expect(semver.eql('0.22.288')).to.be(false);
    });
  });
});
