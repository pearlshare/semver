# Semver

Work with semantic version numbers.

## Parse

```javascript
    var Semver = require("semver");

    var semver = new Semver("1.2.3");

    semver.major; // 1
    semver.minor; // 1
    semver.patch; // 3
```

## Compare

```javascript
    semver.gt('0.1.2'); // true
    semver.gte('0.1.2'); // true
    semver.lt('0.1.2'); // false
    semver.lte('0.1.2'); // false
    semver.eql('1.2.3'); // true
```

See tests for full details.